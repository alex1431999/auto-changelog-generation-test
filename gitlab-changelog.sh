# Considering that your app version is stored in VERSION.md file
#
# Example: ./gitlab-changelog.sh
# Example: ./gitlab-changelog.sh <YOUR-GITLAB-PROJECT-ACCESS-TOKEN>

#!/bin/bash

app_version=1.0.6
from="44c9b2e3e72da33046341c860e2bb95136a370e3"
branch=main
url=https://gitlab.com/api/v4/projects/42952791/repository/changelog

# You can retrieve env variables from script arguments (here 1st argument)
GITLAB_PROJECT_ACCESS_TOKEN=$1

response=$(curl --write-out '%{http_code}' --request POST --header "PRIVATE-TOKEN: ${GITLAB_PROJECT_ACCESS_TOKEN}" --data "version=${app_version}&branch=${branch}&from=${from}" "${url}")

echo $response